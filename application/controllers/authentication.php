<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authentication extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		require_once('\xampp\htdocs\unituition\application\libraries\PhpConsole.php');
		PhpConsole::start();

	}

	public function logout()
	{
		$this->SessionManager->destroySession();
                redirect('/home');

	}

	 public function federatedLogin()
        {
                $token = $this->input->post('token');

                $apiKey = 'YyBP7Lnh44eNQdGhegpRKHOzKnFMnPBnhdjAHmvsy01OPrXtIQYUohCDmHgjFxQb';

                $url = 'https://api.idfederate.com/api/v1/auth_info';

                $fields = array(
                        'apiKey' => urlencode($apiKey),
                        'token' => urlencode($token)
                        );

                $jsonheader = array ( 
                        "Accept: application/json", 
                        "Content-type:application/json"
                        ) ;

                $fields_string = '';

                foreach ($fields as $key => $value) 
                {
                        $fields_string .= $key.'='.$value.'&';
                }

                rtrim($fields_string, '&');

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, count($fields));
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                curl_setopt($ch, CURLOPT_HTTPHEADER,array ("Accept: application/json"));

                $result = curl_exec($ch);
                curl_close($ch);

                $responseArray =  json_decode($result, true);

                foreach($responseArray as $pKey => $pVal)
                {
                        if(is_array($pVal))
                        {
                                foreach ($pVal as $nKey => $nVal) 
                                {
                                        if(is_array($nVal))
                                        {
                                                foreach ($nVal as $nIKey => $nIVal) 
                                                {
                                                        switch($nIKey)
                                                        {
                                                                case "givenName":
                                                                        $fName = $nIVal;
                                                                        break;

                                                                case "familyName":
                                                                        $lName = $nIVal;
                                                                        break;
                                                        }
                                                }
                                        }
                                        else
                                        {
                                                switch($nKey)
                                                {
                                                        case "providerName":
                                                                $provider = $nVal;
                                                                break;

                                                        case "email":
                                                                $email = $nVal;
                                                                break;

                                                        case "birthday":
                                                                $dob = $nVal;
                                                                break;

                                                        case "gender":
                                                                $gender = $nVal;
                                                                break;
                                                }
                                        }
                                }
                        }
                        
                }

                if(!isset($dob))
                        $dob = "NOT_AVAILABLE";
                if(!isset($gender))
                        $gender = "NOT_AVAILABLE";
                if(empty($fname))
                        $fname = $email;
                if(empty($lname))
                        $lname = $email;
                if(!isset($provider))
                {
                        $provider = "NCL";
                        $isTutor = True;
                }
                else
                	$isTutor = false;

                $country = "FEDERATED";
                $password = "FEDERATED";
                $federatedLogin = TRUE;

                $userExist = $this->UserInfo->userExists($email);
                if(!$userExist)
                {
                        $this->UserInfo->addUser($fname, $lname, $email, $provider, $federatedLogin, $dob, $gender, $isTutor);
                        $this->SessionManager->setUserSession($email);
                        
                }
                else
                {
                        $this->SessionManager->setUserSession($email);

                }

                redirect('/profilepage');
        }
}