<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		require_once('\xampp\htdocs\CI\application\libraries\PhpConsole.php');
        PhpConsole::start();
	}

	public function index()
	{
		 $email = $this->SessionManager->getSessionEmail();//$this->session->flashdata('userEmail');

                if($email != "")
                {
                        $data = array();
                        $data['userEmail'] = $email;
                        $this->twig->display('/home/home.html', $data);
                }
                else
                        $this->twig->display('/home/home.html');
	}
}