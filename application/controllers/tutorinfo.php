<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tutorinfo extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->twig->display('/tutorinfo/tutorinfo.html');
	}
}