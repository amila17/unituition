<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->twig->display('/category/category.html');
	}

	public function addCategory()
	{
		$this->UT_category->addCategory('Sports', 'Table Tennis');
		$this->UT_category->addCategory('Sports', 'Badminton');
		$this->UT_category->addCategory('Sports', 'Basketball');
		$this->UT_category->addCategory('Subjects', 'Physics');
		$this->UT_category->addCategory('Subjects', 'Mathematics');
		$this->UT_category->addCategory('Language', 'Chinese');
		$this->UT_category->addCategory('Language', 'French');
		$this->UT_category->addCategory('Language', 'Itlian');
		$this->UT_category->addCategory('Arts', 'Music');
		$this->UT_category->addCategory('Arts', 'Dancing');

		redirect('/home');
	}

	public function findCategoryByCatId()
	{
		$catId = $this->input->post('catId');
		$categoryInfo = $this->UT_category->findCategoryByCatId($catId);
		$data = array();
		$data['categoryInfo'] = $categoryInfo;
		redirect('', $data);
	}

	public function findCategoryByCName()
	{
		$cname = $this->input->post('cname');
		$categoryInfo = $this->UT_category->findCategoryByCName($cname);
		$data = array();
		$data['categoryInfo'] = $categoryInfo;
		redirect('', $data);
	}

	public function findCategoryBySubCname()
	{
		$subCName = $this->input->post('subCName');
		$categoryInfo = $this->UT_category->findCategoryBySubCname($subCName);
		$data = array();
		$data['categoryInfo'] = $categoryInfo;
		redirect('', $data);
	}
}