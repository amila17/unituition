<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profilepage extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		require_once('\xampp\htdocs\CI\application\libraries\PhpConsole.php');
		PhpConsole::start();
	}

	public function index()
	{
		 $email = $this->SessionManager->getSessionEmail();//$this->session->flashdata('userEmail');

                if($email != "")
                {	
                        $user = $this->UserInfo->findUserByEmail($email);

                        $data = array();
                        $data['userEmail'] = $email;
                        $data['fname'] = $user->fname;
                        $data['lname'] = $user->lname;
                        $data['dob'] = $user->dob;
                        $data['email'] = $user->email;
                        $data['gender'] = $user->gender;


                        if (strpos($email,'ncl.ac.uk') !== false) 
                        {

                                $this->twig->display('/profilePage/profilePage.html', $data);
                        }
                        else
                        {
                                $this->twig->display('/profilePage/tuteeProfilePage.html', $data);
                        }
                }
                else
                {
                        redirect('/home');
                }
	}
}