<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		 $sessionEmail = $this->SessionManager->getSessionEmail();//$this->session->flashdata('userEmail');

        if($sessionEmail != "")
        {
                $data = array();
                $data['userEmail'] = $sessionEmail;
        }

		$email = $this->input->post('email');

		$date = $this->input->post('date');
		$time = $this->input->post('time');
		$speciality = $this->input->post('class');

		$tutor = $this->UserInfo->findUserByEmail($email);
		$tutee = $this->UserInfo->findUserByEmail($this->SessionManager->getSessionEmail());


		$data['tutorName'] = $tutor->fname.' '.$tutor->lname;
		$data['tuteeName'] = $tutee->fname.' '.$tutee->lname;
		$data['class'] = $speciality;
		$data['date'] = $date;
		$data['time'] = $time;
		$data['amount'] = $tutor->ratePerHour + 3;
		$bookId = new DateTime();
		$data['bookingId'] = $bookId->getTimestamp().''.rand(0, 1000000000);;

		$this->twig->display('/payment/payment.html', $data);
	}
}