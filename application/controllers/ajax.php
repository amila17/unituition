<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajax extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		require_once('\xampp\htdocs\CI\application\libraries\PhpConsole.php');
		PhpConsole::start();
	}

	public function getTutors()
	{
		$catId = $this->input->get('catId');
		$email = $this->input->post('email');
		
		if(!empty($email))
		{
			$tutor = $this->UserInfo->findUserByEmail($email);

			$this->session->set_flashdata('name', $tutor->fname.' '.$tutor->lname);
			$this->session->set_flashdata('gender', $tutor->gender);
			$this->session->set_flashdata('email', $tutor->email);
			$this->session->set_flashdata('cat', $tutor->ownCategory);
			$this->session->set_flashdata('avaiTime', $tutor->ownAvailablei);
		}

		if(!empty($catId))
		{
			$category = $this->UT_category->loadCategory($catId);

			$profiles = $this->UserInfo->loadUser($category->profile_id);

			$this->session->set_flashdata('name', $profiles->fname.' '.$profiles->lname);
			$this->session->set_flashdata('gender', $profiles->gender);
			$this->session->set_flashdata('email', $profiles->email);
			$this->session->set_flashdata('cat', $profiles->ownCategory);
			$this->session->set_flashdata('avaiTime', $profiles->ownAvailablei);
		}

		redirect('/tutorsearch');	
	}

	public function editUser()
	{
		$fname = $this->input->post('fname');
		$lname = $this->input->post('lname');
		$dob = $this->input->post('dob');
		$gender = $this->input->post('gender');
		$email = $this->input->post('email');

		$ratePerHour = $this->input->post('ratePerHour');
		$speciality = $this->input->post('speciality');
		$availabileDate = $this->input->post('avalabileDate');
		$availableTime = $this->input->post('availableTime');

		

		$this->UserInfo->editUser($email, $fname, $lname, $gender, $dob, $ratePerHour, $speciality, $availabileDate, $availableTime);

	}

	public function editTutee()
	{
		$fname = $this->input->post('fname');
		$lname = $this->input->post('lname');
		$dob = $this->input->post('dob');
		$gender = $this->input->post('gender');
		$email = $this->input->post('email');

		$ratePerHour = 0;
		$speciality = null;
		$availabileDate = null;
		$availableTime = null;

	
		$this->UserInfo->editUser($email, $fname, $lname, $gender, $dob, $ratePerHour, $speciality, $availabileDate, $availableTime);

	}
}