<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tutorsearch extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		require_once('D:\xampp\htdocs\CI\application\libraries\PhpConsole.php');
		PhpConsole::start();
	}

	public function index()
	{
		$TableTennis=$this->UT_category->findCategoryBySubCName('Table Tennis');
		$Badminton=$this->UT_category->findCategoryBySubCName('Badminton');
		$Basketball=$this->UT_category->findCategoryBySubCName('Basketball');

		$Physics=$this->UT_category->findCategoryBySubCName('Physics');
		$Mathematics=$this->UT_category->findCategoryBySubCName('Mathematics');
		$Chinese=$this->UT_category->findCategoryBySubCName('Chinese');
		$French=$this->UT_category->findCategoryBySubCName('French');
		$Itlian=$this->UT_category->findCategoryBySubCName('Itlian');
		$Music=$this->UT_category->findCategoryBySubCName('Music');
		$Dancing=$this->UT_category->findCategoryBySubCName('Dancing');

		$data = array();
		$data['ttID'] = $TableTennis->id;
		$data['badmintonID'] = $Badminton->id;
		$data['basketBallID'] = $Basketball->id;
		
		$data['name'] = $this->session->flashdata('name');
		$data['gender'] = $this->session->flashdata('gender');
		$data['email'] = $this->session->flashdata('email');


		$cat = array();
		$cat = $this->session->flashdata('cat');

		if(!empty($cat))
		{
			foreach ($cat as &$value) 
			{
				$value = $value->subCName;
			}
			$data['specialities'] =  $value;
		}


		$avaiTime = array();
		$avaiTime = $this->session->flashdata('avaiTime');

		if(!empty($avaiTime))
		{
			foreach($avaiTime as &$avaiValue)
			{
				$data['date'] =  $avaiValue->date;
				$data['time'] =  $avaiValue->time;
			}

		}	

		$sessionEmail = $this->SessionManager->getSessionEmail();//$this->session->flashdata('userEmail');

        if($sessionEmail != "")
        {
                $data['userEmail'] = $sessionEmail;
        }

		$this->twig->display('/tutorsearch/tutorsearch.html', $data);
	}

}