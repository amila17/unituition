<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class SessionManager extends CI_Model
{
	public function __construct()
	{
/*		require_once('D:\xampp\htdocs\CI\application\libraries\PhpConsole.php');
		PhpConsole::start();*/
	}

	public function setUserSession($email)
	{
		$date = new DateTime();

		$userData = array(
				'email' => $email,
				'loggedIn' => TRUE,
				'timestamp' => $date->getTimestamp(),
					);

		$this->session->set_userdata($userData);
	}

	public function userSessionExist($email)
	{
		$sessionEmail = $this->session->userdata('email');
		$loggedIn = $this->session->userdata('loggedIn');

		if(!$sessionEmail && !$loggedIn)
			return FALSE;
		elseif($loggedIn = TRUE)
		{
			return TRUE;
		}
	}

	public function destroySession()
	{
		$this->session->sess_destroy();
	}

	public function getSessionEmail()
	{
		$sessionEmail = $this->session->userdata('email');

		if(!$sessionEmail)
			$sessionEmail = "";

		return $sessionEmail;
	}

}
