<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class UT_category extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function addCategory($cname, $subCname)
	{
		$date = new DateTime();
		$categoryInfo = R::dispense('category');
		$categoryInfo->catId = $cname.$date->getTimestamp();
		$categoryInfo->cname = $cname; //sport
		$categoryInfo->subCName = $subCname;

		R::store($categoryInfo);
	}

	public function findCategoryByCatId($catId)
	{
		$categoryInfo = R::findOne('category', ' catId = :catId ', array( ':catId'=>$catId ));

		return $categoryInfo;
	}

	public function loadCategory($catId)
	{
		$categoryInfo = R::load('category', $catId);

		return $categoryInfo;
	}

	public function findCategoryByCName($cname)
	{
		$categoryInfo = R::load('category', 'cname = :cname', array( ':cname'=>$cname ));

		return $categoryInfo;
	}

	public function findCategoryBySubCName($subCName)
	{
		$categoryInfo = R::findOne('category', 'subCName = :subCName', array( ':subCName'=>$subCName ));
		return $categoryInfo;
	}

	public function removeCategoryById($catId)
	{
		$categoryInfo = R::load('category', $catId);

		R::trash($categoryInfo);
	}

}