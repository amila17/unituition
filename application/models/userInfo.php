<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class UserInfo extends CI_Model
{
	public function __construct()
	{	
		/*require_once('D:\xampp\htdocs\CI\application\libraries\PhpConsole.php');
		PhpConsole::start();*/
	}

	public function addUser($fname, $lname, $email, $provider, $federatedLogin, $dob, $gender, $isTutor)
	{
		$userInformation = R::dispense('profile');

		$userInformation->fname = $fname;
		$userInformation->lname = $lname;
		$userInformation->email = $email;
		$userInformation->federatedLogin = $federatedLogin;
		$userInformation->provider = $provider;
		$userInformation->dob = $dob;
		$userInformation->gender = $gender;
		$userInformation->isTutor = $isTutor;

		$userInformation->ratePerHour = 0;


		R::store($userInformation);

	}

	public function editUser($email, $fname, $lname, $gender, $dob, $ratePerHour, $speciality, $availableDate, $availableTime)
	{

		$userInformation = R::findOne('profile', ' email = :email ', array(
																		':email'=>$email
																		));

		if(!empty($availableDate))
		{
			$availableInfo = R::dispense('availablei');
			$availableInfo->date = $availableDate;
			$availableInfo->time = $availableTime;

			$availableid = R::store($availableInfo);

			$avaiInfo = R::load('availablei', $availableid );
			//Available Info
			if(is_null($userInformation->ownAvailablei))
			{
				$userInformation->ownAvailablei = array($avaiInfo);
				$userInformation->availableInfos = array($avaiInfo->id);
			}
			else
			{
				$userInformation->ownAvailablei[] = $avaiInfo;
				$userInformation->availableInfos[] = $avaiInfo->id;
			}
		}



		$userInformation->fname = $fname;
		$userInformation->lname = $lname;
		$userInformation->gender = $gender;
		$userInformation->dob = $dob;
		$userInformation->ratePerHour = $ratePerHour;

		if(!is_null($speciality))
			$category = $this->UT_category->findCategoryBySubCName($speciality);

		//Speciality
		if(!is_null($speciality))
		{
			if(is_null($userInformation->ownCategory))
			{
				$userInformation->ownCategory = array($category);
				$userInformation->categoryIds = array($category->id);
			}
			else
			{
				$userInformation->ownCategory[] = $category;
				$userInformation->categoryIds[] = $category->id;
			}
		}

		R::store($userInformation);
	}

	public function findUserByEmail($email)
	{

		$userInformation = R::findOne('profile', ' email = :email ', array(
																		':email'=>$email
																		));
		return $userInformation;
	}

	public function findUserBySpeciality($speciality)
	{

		$userInformation = R::find('profile', ' speciality = :speciality AND isTutor = :isTutor', array(
																		':speciality'=>$speciality,
																		':isTutor'=>1,
																		));

		return $userInformation;
	}

	 public function userExists($email)
    {
            $userExist = FALSE;

            $userProfile = $this->findUserByEmail($email);

            if(!empty($userProfile))
                    $userExist = TRUE;

            return $userExist;
    }

    public function loadUser($userId)
	{

		$user = R::load('profile', $userId);
		return $user;
	}

	public function loadAvaiTime($avaiId)
	{
		$avai = R::load('availablei', $avaiId);

		return $avai;
	}


}	